FROM ubuntu:18.04

WORKDIR /app

COPY . /app

RUN apt-get update  && \
    apt-get -y upgrade  && \
    apt-get -y install cmake  && \
    apt-get -y install g++  && \
    apt-get -y install libpng-dev  && \
    apt-get -y install libtiff-dev  && \
    cd sampletool  && \
    mkdir build  && \
    cd build  && \
    cmake ..  && \
    make  && \
    cd ..
    
ENV PATH="/app/sampletool/sampletool/build/bin:$PATH"    

CMD ["bash"]
   
