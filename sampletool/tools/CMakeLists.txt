############################################################
#
# $Id$
#
# Copyright (c) cimgdenoising 2018
#
# AUTHOR:
# Sylvain Prigent
# 

# Project name
project(stTools)

## #################################################################
## Define files
## #################################################################
include_directories(${cimgdenoising_SRC_INCLUDE_DIR})

SET(SRC_FILES 
	)

## #################################################################
## Exe generation
## #################################################################
SET(NAME meanfilter)
SET(FILE ${SRC_FILES} meanfilter.cpp)
add_executable( ${NAME} ${FILE} )
target_link_libraries(${NAME} sampletool ${CIMG_LIBRARIES})
