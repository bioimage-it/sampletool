#include <sampletool>
#include <iostream>

using namespace cimg_library;

int main(int argc, char **argv)
{
  cimg_usage("Smooth a 2D gray scale image with a mean filter");

  const char *file_i    = cimg_option("-i",(char*)NULL,"Input image");
  const char *file_o    = cimg_option("-o",(char*)NULL,"Output file");
  const int radius    = cimg_option("-r", 3, "Filter radius (in pixels)");


  if (!file_i || !file_o){
      std::cout << "Input file or output file missing" << std::endl;
      return 0;
  }

  CImg<float> source(file_i);
  CImg<float> destination = meanfilter(source, radius);
  destination.save(file_o);

  return 0;
}
