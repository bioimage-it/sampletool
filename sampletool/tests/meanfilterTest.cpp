#include <sampletool>
#include "stTestConfig.h"

#include <iostream>

int main(int argc, char *argv[])
{

    std::cout << "input image = " << CELEGANS << std::endl;
    std::cout << "result image = " << CELEGANSRESULT << std::endl;

    // apply mean filter
    cimg_library::CImg<float> src(CELEGANS);
    cimg_library::CImg<float> res = meanfilter(src, 3);

    // calculate error with the reference image
    cimg_library::CImg<float> ref(CELEGANSRESULT);
    float error = ref.MSE(res);
    std::cout << "mse = " << error << std::endl;
    if (error > 10)
    {
        return 1;
    }
    return 0;

}
