
#pragma once

#include "CImg.h"

cimg_library::CImg<float> meanfilter(const cimg_library::CImg<float> &image, int radius);