#include "meanfilter.h"

cimg_library::CImg<float> meanfilter(const cimg_library::CImg<float> &image, int radius){

    float mean = 0.0;
    float coeff = (2*radius+1)*(2*radius+1);
    int width = image.width();
    int height = image.height();
    cimg_library::CImg<float> destination(width, height); 
    destination.fill(0);

    for (int x = radius ; x < width-radius ; x++){
       for (int y = radius ; y < height-radius ; y++){
            mean = 0.0;
            for (int i = -radius ; i <= radius ; i++ ){
                for (int j = -radius ; j <= radius ; j++ ){
                    mean += image(x+i, y+j);
                }     
            }    
            mean /= coeff;
            destination(x,y) = mean;
        } 
    }
    return destination;
}
