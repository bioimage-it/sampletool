# Sampletool

Sample tool is an example repository for automating the build and share of a BioImage-IT compatible image processing tool

The repository is organized as follows:
1. `sampletool/`: contains the source code of tool. A 2D mean image filter in this example
2. `tools/`: tools wrappers. Wrappers are XML files that descibes the inputs, outputs and parameters of the image processing tools. In this example there is a singel wrapper *sampletool.xml*
3. `public/` the directory that contains the documentation web pages. This is a user, and data driven documentation. Not a dev documentation
4. `Dockerfile` the dockerfile to generate the docker image


# Docker image

The docker image is availabel at: 
[registry.gitlab.inria.fr/bioimage-it/sampletool:766efe8b8397fef0115e20f8e7cd2853a0e0e0d5](registry.gitlab.inria.fr/bioimage-it/sampletool:766efe8b8397fef0115e20f8e7cd2853a0e0e0d5)

# Documentation

The documentation is available at:
[https://bioimage-it.gitlabpages.inria.fr/sampletool/](https://bioimage-it.gitlabpages.inria.fr/sampletool/)
